package org.training.robot;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class RectangleTest {

    @Test
    public void overlapsWith_TopLeftOver() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(95, 95, 10, 10);

        assertTrue(first.overlapsWith(second));
        assertEquals(-1.0, first.distance(second), 0.0);
    }

    @Test
    public void overlapsWith_TopRightOver() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(105, 95, 10, 10);

        assertTrue(first.overlapsWith(second));
        assertEquals(-1.0, first.distance(second), 0.0);
    }

    @Test
    public void overlapsWith_BottomLeftOver() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(95, 105, 10, 10);

        assertTrue(first.overlapsWith(second));
        assertEquals(-1.0, first.distance(second), 0.0);
    }

    @Test
    public void overlapsWith_BottomRightOver() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(105, 105, 10, 10);

        assertTrue(first.overlapsWith(second));
        assertEquals(-1.0, first.distance(second), 0.0);
    }

    @Test
    public void overlapsWith_MiddleIn() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(102, 102, 6, 6);

        assertTrue(first.overlapsWith(second));
        assertEquals(-1.0, first.distance(second), 0.0);
    }

    @Test
    public void overlapsWith_MiddleOut() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(98, 98, 12, 12);

        assertTrue(first.overlapsWith(second));
        assertEquals(-1.0, first.distance(second), 0.0);
    }

    @Test
    public void overlapsWith_TopLeft() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(80, 80, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void overlapsWith_TopMiddle() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(100, 80, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void overlapsWith_TopRight() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(120, 80, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void overlapsWith_BottomLeft() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(80, 120, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void overlapsWith_BottomMiddle() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(100, 120, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void overlapsWith_BottomRight() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(120, 120, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void overlapsWith_Left() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(80, 100, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void overlapsWith_Right() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(120, 100, 10, 10);

        assertFalse(first.overlapsWith(second));
    }

    @Test
    public void distance_TopLeft() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(80, 80, 10, 10);

        assertEquals(14.142136, first.distance(second), 0.000001);
    }

    @Test
    public void distance_TopMiddle() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(100, 80, 10, 10);

        assertEquals(10.0, first.distance(second), 0.0);
    }

    @Test
    public void distance_TopRight() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(120, 80, 10, 10);

        assertEquals(14.142136, first.distance(second), 0.000001);
    }

    @Test
    public void distance_BottomLeft() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(80, 120, 10, 10);

        assertEquals(14.142136, first.distance(second), 0.000001);
    }

    @Test
    public void distance_BottomMiddle() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(100, 120, 10, 10);

        assertEquals(10.0, first.distance(second), 0.0);
    }

    @Test
    public void distance_BottomRight() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(120, 120, 10, 10);

        assertEquals(14.142136, first.distance(second), 0.000001);
    }

    @Test
    public void distance_Left() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(80, 100, 10, 10);

        assertEquals(10.0, first.distance(second), 0.0);
    }

    @Test
    public void distance_Right() {
        Rectangle first = new Rectangle(100, 100, 10, 10);
        Rectangle second = new Rectangle(120, 100, 10, 10);

        assertEquals(10.0, first.distance(second), 0.0);
    }

}
