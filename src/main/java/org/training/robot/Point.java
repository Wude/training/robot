package org.training.robot;

public class Point {

    private final int x;
    private final int y;

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(Point other) {
        return this.distance(other.x, other.y);
    }

    public double distance(int otherX, int otherY) {
        return distance(this.x, this.y, otherX, otherY);
    }

    public static double distance(int thisX, int thisY, int otherX, int otherY) {
        return Math.abs(Math.sqrt(Math.pow(thisX - otherX, 2) + Math.pow(thisY - otherY, 2)));
    }

}
