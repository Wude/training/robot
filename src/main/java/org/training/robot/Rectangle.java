package org.training.robot;

import java.awt.Color;

public class Rectangle {

    private final Point position;
    private final int width;
    private final int height;
    private final Color color;
    private final String description;

    public Point getPosition() {
        return this.position;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public Color getColor() {
        return this.color;
    }

    public String getDescription() {
        return this.description;
    }

    public Rectangle(Point position, int width, int height, Color color, String description) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.color = color;
        this.description = description;
    }

    public Rectangle(int x, int y, int width, int height, Color color, String description) {
        this(new Point(x, y), width, height, color, description);
    }

    public Rectangle(int x, int y, int width, int height, Color color) {
        this(x, y, width, height, color, "");
    }

    public Rectangle(int x, int y, int width, int height) {
        this(x, y, width, height, Color.BLACK);
    }

    public boolean overlapsWith(Rectangle other) {
        int thisTopLeftX = this.position.getX();
        int thisTopLeftY = this.position.getY();
        int thisBottomRightX = thisTopLeftX + this.width;
        int thisBottomRightY = thisTopLeftY + this.height;

        int otherTopLeftX = other.position.getX();
        int otherTopLeftY = other.position.getY();
        int otherBottomRightX = otherTopLeftX + other.width;
        int otherBottomRightY = otherTopLeftY + other.height;

        HorizontalPositioning hPos = thisTopLeftX > otherBottomRightX ? HorizontalPositioning.LEFT
                : (otherTopLeftX > thisBottomRightX ? HorizontalPositioning.RIGHT : HorizontalPositioning.MIDDLE);
        VerticalPositioning vPos = thisTopLeftY > otherBottomRightY ? VerticalPositioning.TOP
                : (otherTopLeftY > thisBottomRightY ? VerticalPositioning.BOTTOM : VerticalPositioning.MIDDLE);

        return hPos == HorizontalPositioning.MIDDLE && vPos == VerticalPositioning.MIDDLE;
    }

    public double distance(Rectangle other) {
        int thisTopLeftX = this.position.getX();
        int thisTopLeftY = this.position.getY();
        int thisBottomRightX = thisTopLeftX + this.width;
        int thisBottomRightY = thisTopLeftY + this.height;

        int otherTopLeftX = other.position.getX();
        int otherTopLeftY = other.position.getY();
        int otherBottomRightX = otherTopLeftX + other.width;
        int otherBottomRightY = otherTopLeftY + other.height;

        HorizontalPositioning hPos = thisTopLeftX > otherBottomRightX ? HorizontalPositioning.LEFT
                : (otherTopLeftX > thisBottomRightX ? HorizontalPositioning.RIGHT : HorizontalPositioning.MIDDLE);
        VerticalPositioning vPos = thisTopLeftY > otherBottomRightY ? VerticalPositioning.TOP
                : (otherTopLeftY > thisBottomRightY ? VerticalPositioning.BOTTOM : VerticalPositioning.MIDDLE);

        double distance = -1; // Defaults to indicate overlapping rectangles.

        switch (hPos) {
        case LEFT:
            switch (vPos) {
            case TOP:
                // The rectangles don't overlap.
                // The other rectangle is positioned to the top left of this rectangle.
                distance = Point.distance(thisTopLeftX, thisTopLeftY, otherBottomRightX, otherBottomRightY);
                break;
            case MIDDLE:
                // The rectangles don't overlap.
                // The other rectangle is positioned exactly left of this rectangle.
                distance = thisTopLeftX - otherBottomRightX;
                break;
            case BOTTOM:
                // The rectangles don't overlap.
                // The other rectangle is positioned to the bottom left of this rectangle.
                distance = Point.distance(thisTopLeftX, thisBottomRightY, otherBottomRightX, otherTopLeftY);
                break;
            }
            break;
        case MIDDLE:
            switch (vPos) {
            case TOP:
                // The rectangles don't overlap.
                // The other rectangle is positioned exactly over this rectangle.
                distance = thisTopLeftY - otherBottomRightY;
                break;
            case MIDDLE:
                // The rectangles overlap.
                break;
            case BOTTOM:
                // The rectangles don't overlap.
                // The other rectangle is positioned exactly under this rectangle.
                distance = otherTopLeftY - thisBottomRightY;
                break;
            }
            break;
        case RIGHT:
            switch (vPos) {
            case TOP:
                // The rectangles don't overlap.
                // The other rectangle is positioned to the top right of this rectangle.
                distance = Point.distance(thisBottomRightX, thisTopLeftY, otherTopLeftX, otherBottomRightY);
                break;
            case MIDDLE:
                // The rectangles don't overlap.
                // The other rectangle is positioned exactly right of this rectangle.
                distance = otherTopLeftX - thisBottomRightX;
                break;
            case BOTTOM:
                // The rectangles don't overlap.
                // The other rectangle is positioned to the bottom right of this rectangle.
                distance = Point.distance(thisBottomRightX, thisBottomRightY, otherTopLeftX, otherTopLeftY);
                break;
            }
            break;
        }

        return distance;
    }

}
