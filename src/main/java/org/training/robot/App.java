package org.training.robot;

import java.util.List;

public abstract class App {

    private App() {
    }

    public static void main(String[] args) {
        GameField gameField = new GameField();
        List<Rectangle> obstacles = gameField.generateObstacles();
        gameField.draw(obstacles);
    }

}
