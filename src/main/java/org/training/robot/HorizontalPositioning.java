package org.training.robot;

public enum HorizontalPositioning {

    LEFT, MIDDLE, RIGHT;

}
