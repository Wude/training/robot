package org.training.robot;

import java.awt.Color;
import javax.swing.JFrame;

/**
 * Screen/Canvas ("Leinwand").
 */
public class Screen {

    private final JFrame window;
    private final DrawingArea drawingArea;
    private Color backgroundColor;
    private int width;
    private int height; // length?

    public DrawingArea getDrawingArea() {
        return this.drawingArea;
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void wait(int milliseconds) {
    }

    public void update() {
        this.window.invalidate();
        this.window.validate();
        this.window.repaint();
    }

    public Screen(int width, int height) {
        this.window = new JFrame();
        this.drawingArea = new DrawingArea();
        this.window.add(this.drawingArea);
        this.backgroundColor = Color.WHITE;
        this.width = width;
        this.height = height;
        this.window.setSize(width, height);
        this.window.setVisible(true);
        this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
