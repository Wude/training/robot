package org.training.robot;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 * Drwaing area ("Zeichenfläche").
 */
public class DrawingArea extends JPanel {

    private static final long serialVersionUID = -8265751199444476726L;

    private List<Rectangle> obstacles = new ArrayList<>();

    public List<Rectangle> getObstacles() {
        return this.obstacles;
    }

    public void setObstacles(List<Rectangle> values) {
        this.obstacles = values;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Rectangle obstacle : obstacles) {
            g.setColor(obstacle.getColor());
            g.drawRect(obstacle.getPosition().getX(), obstacle.getPosition().getY(), obstacle.getWidth(),
                    obstacle.getHeight());
            g.fillRect(obstacle.getPosition().getX(), obstacle.getPosition().getY(), obstacle.getWidth(),
                    obstacle.getHeight());
        }
    }

}
