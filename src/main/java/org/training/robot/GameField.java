package org.training.robot;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.awt.Color;

/**
 * Game field ("Spielfeld").
 */
public class GameField {

    private static final ThreadLocalRandom random = ThreadLocalRandom.current();

    private final Screen screen;

    public GameField() {
        this.screen = new Screen(1200, 900);
    }

    private static Color generateColor() {
        return new Color(random.nextInt(0, 255), random.nextInt(0, 255), random.nextInt(0, 255));
    }

    public List<Rectangle> generateObstacles() {
        int obstacleCount = 2000;
        List<Rectangle> obstacles = new ArrayList<>();
        int maxWidth = this.screen.getDrawingArea().getWidth();
        int maxHeight = this.screen.getDrawingArea().getHeight();
        int attempts = 0;
        for (int i = 0; i < obstacleCount; attempts++) {
            int x = random.nextInt(0, maxWidth);
            int y = random.nextInt(0, maxHeight);
            int width = random.nextInt(0, Math.min(100, maxWidth - x));
            int height = random.nextInt(0, Math.min(100, maxHeight - y));
            Rectangle rectangle = new Rectangle(x, y, width, height, generateColor(), "");
            boolean skip = false;
            for (Rectangle obstacle : obstacles) {
                if (obstacle.overlapsWith(rectangle)) {
                    skip = true;
                    break;
                }
            }
            if (!skip) {
                obstacles.add(rectangle);
                i++;
            }
        }
        // javax.swing.JOptionPane.showMessageDialog(null,
        // java.text.MessageFormat.format("attempts: {0}", attempts));
        System.out.print("attempts: ");
        System.out.println(attempts);
        return obstacles;
    }

    public void draw(List<Rectangle> obstacles) {
        screen.getDrawingArea().setObstacles(obstacles);
        this.screen.update();
    }

}
