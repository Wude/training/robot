package org.training.robot;

public enum VerticalPositioning {

    TOP, MIDDLE, BOTTOM;

}
